($ => {
    $(function () {
        // PRELOADER
        if (sessionStorage.getItem("isFirstRun")) { $("#preloader").addClass("d-none"); }
        $(window).on("load", function (e) { $("#preloader").fadeOut(); this.sessionStorage.setItem("isFirstRun", true); });
        setTimeout(function () { $("#preloader").fadeOut(); this.sessionStorage.setItem("isFirstRun", true); }, 4000);

    });
})(jQuery);